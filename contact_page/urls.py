from django.urls import path
from .views import contact, saveMessage

urlpatterns = [
    path('', contact, name="contact"),
    path('message/', saveMessage, name="message")
]
