from django import forms

class FormMessage(forms.Form):
    message = forms.CharField(required=True, max_length=200, widget=forms.Textarea())