from django.shortcuts import render
from .forms import FormMessage
from .models import DataMessage
from django.http import JsonResponse


# Create your views here.

def contact(request):
    form = FormMessage()
    data = list(DataMessage.objects.all())
    data.reverse()
    return render(request, 'contactme_page/contact-page.html', {'form': form, 'data': data})


def saveMessage(request):
    if request.method == 'POST':
        message = request.POST['message']
        DataMessage.objects.create(message=message)
        return JsonResponse({'saved': True, 'message': message})
    return JsonResponse({'saved': False})
