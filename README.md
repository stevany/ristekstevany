Heroku App
-----------
http://hany-love-web-dev.herokuapp.com/

Website's Guide
----
Please read the instruction below for run it on your local machine.

Requirements
------------
1. Python 3
2. PIP
3. Django

Installation 
------------
#### For Mac or Linux:
##### Go to the project path using command:
```
cd (project directory)
```
##### Go to the project path and create VirtualENV using command:
```
python3 -m virtualenv env
```
##### Activate the env using command:
```
source env/bin/activate
```
##### Install the requirements:
```
pip3 install -r requirements.txt
```
##### Run Django server using command:
```
python3 manage.py runserver
```

##### Now check your browser and go to:
```
127.0.0.1:8000 or localhost:8000
```
<br />

#### For Windows:
##### In your Command Prompt navigate to Desktop :
```
pip install virtualenv
```
##### In your Command Prompt navigate to your project:
```
cd (project directory)
```
##### Within your project:
```
virtualenv env
```
##### On windows, virtualenv creates a batch file
```
\env\Scripts\activate.bat
```
##### to activate virtualenv on Windows, activate script is in the Scripts folder :
```
\path\to\env\Scripts\activate
```
##### Install the requirements:
```
pip install -r requirements.txt
```
##### Now check your browser and go to:
```
127.0.0.1:8000 or localhost:8000
```

Overview about my website
----

On my website, I use simple and clean concept but still aesthetic. 
On the 'homepage', I put a lot of photos to give a portrait of my life from childhood till now. 
When you entering 'about me' page, I offer different experience for you. 
I made a 'journey' concept where every section has its own action. 
You also can see my portfolio as UI/UX designer on the 'portfolio page' (I hope there're will be my portfolio as a web developer soon).
When you click the pict of my portfolio, you can see the live project. 
On 'contact page', you can give me a message to comment my website. 
Do not hesitate to drop a message! Thankyou!
