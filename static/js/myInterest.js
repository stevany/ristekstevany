$(document).ready(function () {
    //Tooltip
    $('[data-toggle="tooltip"]').tooltip();

    // When the user clicks on the button, scroll to the top of the portfolio page
    $("#arrow-up-contact").click(function () {
        $('html, body').animate({
            scrollTop: $("#portfolio").offset().top
        }, 800);
    });

    window.onscroll = function () {
        scrollFunction();
    };

    function scrollFunction() {
        if (document.body.scrollTop > 600 || document.documentElement.scrollTop > 600) {
            document.getElementById("arrow-up-contact").style.display = "block";
        } else {
            document.getElementById("arrow-up-contact").style.display = "none";
        }
    }

    // When the user clicks on the button, scroll to the top of the about page
    $("#arrow-up-contact").click(function () {
        $('html, body').animate({
            scrollTop: $("#about").offset().top
        }, 800);
    });

    window.onscroll = function () {
        scrollFunction();
    };

    function scrollFunction() {
        if (document.body.scrollTop > 600 || document.documentElement.scrollTop > 600) {
            document.getElementById("arrow-up-contact").style.display = "block";
        } else {
            document.getElementById("arrow-up-contact").style.display = "none";
        }
    }

    //Interest icon on click
    document.getElementById("makeup-icon").onclick = function () {
        makeupFunction()
    };

    function makeupFunction() {
        $('#makeup').fadeIn('slow');
    }

    document.getElementById("movie-icon").onclick = function () {
        movieFunction()
    };

    function movieFunction() {
        $('#movie').fadeIn('slow');
    }

    document.getElementById("travel-icon").onclick = function () {
        travelFunction()
    };

    function travelFunction() {
        $('#travel').fadeIn('slow');
    }

    document.getElementById("photoshoot-icon").onclick = function () {
        photoShootFunction()
    };

    function photoShootFunction() {
        $('#photoshoot').fadeIn('slow');
    }

    //Reveal GPA on click

    document.getElementById("button-gpa").onclick = function () {
        gpaFunction()
    };

    function gpaFunction() {
        $('#reveal-gpa').css('display', 'none');
        $('#hany-gpa').fadeIn('slow').delay(650);
        $('#hany-gpa').fadeOut('slow');
        setTimeout(function () {
            $('#reveal-gpa').fadeIn('slow');
        }, 1900);
    }
});



