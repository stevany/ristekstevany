from django.shortcuts import render

# Create your views here.
def home(request):
    return render(request, 'home_page/home-page.html')

def about(request):
    return render(request, 'about_page/about-page.html')

def portfolio(request):
    return render(request, 'portfolio_page/portfolio-page.html')