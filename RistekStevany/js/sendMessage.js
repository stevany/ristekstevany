$(function () {
    $('#form').on('submit', function (event) {
        event.preventDefault();
        saveMessage();
    });

    function saveMessage() {
        var csrftoken = $("[name=csrfmiddlewaretoken]").val();
        var message = $('#id_message').val();
        $.ajax({
            method: "POST",
            url: "/contact/message/",
            headers: {
                "X-CSRFToken": csrftoken,
            },
            data: {
                message: message,
            },
            success: function (result) {
                if (result.saved) {
                    var html = `<div class='box-message'><p class="font-message">“${result.message}”</p></div>`;
                    $('.show-message').prepend(html);
                    $('#id_message').val('');
                }
            },
        });
    }
});